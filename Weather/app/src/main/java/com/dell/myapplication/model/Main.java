package com.dell.myapplication.model;

import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Main {
    @PrimaryKey
    private int id;
    @SerializedName("humidity")
    @Expose
    private Integer mHumidity;
    @SerializedName("pressure")
    @Expose
    private Double mPressure;
    @SerializedName("temp")
    @Expose
    private Double mTemp;
    @SerializedName("temp_max")
    @Expose
    private Double mTempMax;
    @SerializedName("temp_min")
    @Expose
    private Double mTempMin;

    public Integer getmHumidity() {
        return mHumidity;
    }

    public void setmHumidity(Integer mHumidity) {
        this.mHumidity = mHumidity;
    }

    public Double getmPressure() {
        return mPressure;
    }

    public void setmPressure(Double mPressure) {
        this.mPressure = mPressure;
    }

    public Double getmTemp() {
        return mTemp;
    }

    public void setmTemp(Double mTemp) {
        this.mTemp = mTemp;
    }

    public Double getmTempMax() {
        return mTempMax;
    }

    public void setmTempMax(Double mTempMax) {
        this.mTempMax = mTempMax;
    }

    public Double getmTempMin() {
        return mTempMin;
    }

    public void setmTempMin(Double mTempMin) {
        this.mTempMin = mTempMin;
    }


}
