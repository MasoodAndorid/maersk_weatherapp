package com.dell.myapplication.repository;

import android.content.Context;
import android.os.AsyncTask;

import com.dell.myapplication.dao.MainDao;
import com.dell.myapplication.database.WeatherDatabase;
import com.dell.myapplication.roomModel.MainRoomModel;

public class MainEntityRepo {

    private MainDao mMainDao;
    private WeatherDatabase mWeatherDatabase;


    public MainEntityRepo(Context mContext){
        mWeatherDatabase = WeatherDatabase.getAppDatabase(mContext);
        mMainDao = mWeatherDatabase.getMainDao();
    }

    /**
     *
     * @param model
     */
    public void insert(final MainRoomModel model){
        new AsyncTask<Void, Void, Boolean>(){
            @Override
            protected Boolean doInBackground(Void... voids) {
                mMainDao.insert(model);
                return true;
            }
        }.execute();
    }

    /**
     *
     * @param model
     */
    public void delete(final MainRoomModel model){
        new AsyncTask<Void, Void, Boolean>(){
            @Override
            protected Boolean doInBackground(Void... voids) {
                mMainDao.delete(model);
                return true;
            }
        }.execute();
    }

    public void update(final MainRoomModel model){
        new AsyncTask<Void, Void, Boolean>(){
            @Override
            protected Boolean doInBackground(Void... voids) {
                mMainDao.update(model);
                return true;
            }
        }.execute();
    }

    /**
     *
     * @param weatherId
     */
    public void deleteByWeatherId(final int weatherId){
        new AsyncTask<Void, Void, Boolean>(){
            @Override
            protected Boolean doInBackground(Void... voids) {
                mMainDao.deleteWeatherById(weatherId);
                return true;
            }
        }.execute();
    }

    /**
     *
     */
    public void deleteAllMainTable() {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                mMainDao.deleteAll();
                return true;
            }
        }.execute();
    }
}
