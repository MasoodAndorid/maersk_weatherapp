package com.dell.myapplication.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

@Dao
public interface WeatherWithMainDao {

    @Query("SELECT * FROM weather")
    public LiveData<List<WeatherWithMain>> getWeatherWithMain();

    @Query("SELECT * FROM weather WHERE id =:mId")
    public List<WeatherWithMain> getWeatherById(int  mId);

    @Query("SELECT * FROM weather WHERE name LIKE :mName")
    public List<WeatherWithMain> getWeatherByName(String mName);


}
