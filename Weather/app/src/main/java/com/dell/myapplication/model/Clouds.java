package com.dell.myapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Clouds {
    @SerializedName("all")
    @Expose
    private Integer mAll;

    public Integer getmAll() {
        return mAll;
    }

    public void setmAll(Integer mAll) {
        this.mAll = mAll;
    }
}
