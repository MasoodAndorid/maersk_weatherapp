package com.dell.myapplication.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.dell.myapplication.dao.MainDao;
import com.dell.myapplication.dao.WeatherDao;
import com.dell.myapplication.dao.WeatherWithMainDao;
import com.dell.myapplication.roomModel.MainRoomModel;
import com.dell.myapplication.roomModel.WeatherRoomModel;

@Database(entities = {WeatherRoomModel.class, MainRoomModel.class}, version = 1)
public abstract class WeatherDatabase extends RoomDatabase {

    private static WeatherDatabase INSTANCE;

    public abstract MainDao getMainDao();

    public abstract WeatherDao getWeatherDao();

    public abstract WeatherWithMainDao getWeatherWithMainDao();

    /**
     * @param context
     * @return
     */
    public static WeatherDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), WeatherDatabase.class, "weather_database")
                            // allow queries on the main thread.
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void clearDatabaseInstance() {
        INSTANCE = null;
    }

}
