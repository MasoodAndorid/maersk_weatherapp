package com.dell.myapplication.roomModel;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "weather")
public class WeatherRoomModel {

    @PrimaryKey
    public int id;

    @ColumnInfo(name = "name")
    public String name;

    public WeatherRoomModel(int id, String name){
        this.id = id;
        this.name = name;
    }


}
