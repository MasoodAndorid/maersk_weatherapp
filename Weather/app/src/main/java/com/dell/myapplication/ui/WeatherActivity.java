package com.dell.myapplication.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.dell.myapplication.R;
import com.dell.myapplication.WeatherApplication;
import com.dell.myapplication.adapter.WeatherAdapter;
import com.dell.myapplication.base.BaseActivity;
import com.dell.myapplication.connectivity.Connectivity;
import com.dell.myapplication.database.WeatherDatabase;
import com.dell.myapplication.network.WeatherService;
import com.dell.myapplication.repository.MainEntityRepo;
import com.dell.myapplication.repository.WeatherEntityRepo;
import com.dell.myapplication.dao.WeatherWithMain;
import com.dell.myapplication.repository.WeatherWithMainRepo;
import com.dell.myapplication.model.Main;
import com.dell.myapplication.model.WeatherModel;
import com.dell.myapplication.databinding.ActivityMainBinding;
import com.dell.myapplication.roomModel.MainRoomModel;
import com.dell.myapplication.roomModel.WeatherRoomModel;
import com.dell.myapplication.utils.DialogUtils;
import com.dell.myapplication.viewModel.WeatherViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * author: Masood
 * Date: 05 - Aug -2019
 */

public class WeatherActivity extends BaseActivity implements SearchView.OnQueryTextListener {

    private static final String TAG = WeatherActivity.class.getCanonicalName();
    private ActivityMainBinding mActivityMainBinding;
    Activity mActivity;
    private WeatherService mWeatherService;
    private WeatherViewModel mWeatherViewModel = null;
    private WeatherEntityRepo mWeatherEntityRepo;
    private MainEntityRepo mMainEntityRepo;
    private WeatherWithMainRepo mWeatherWithMainRepo;
    private WeatherAdapter mWeatherAdapter;
    private List<WeatherRoomModel> mWeatherList = null;
    private HashMap<String, Double> mTempList = null;
    AlertDialog alertDialogView = null;

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = WeatherActivity.this;
        mActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(mActivityMainBinding.toolBar.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        }
        mActivityMainBinding.toolBar.toolbarTitle.setText(getString(R.string.toolbar_name));
        initViews();
        initAdapter();
        dbOperations();
        apiResponse();
    }

    private void initViews() {
        mWeatherViewModel = ViewModelProviders.of(this).get(WeatherViewModel.class);
        mWeatherList = new ArrayList<>();
        mTempList = new HashMap<>();
        mWeatherEntityRepo = new WeatherEntityRepo(this);
        mMainEntityRepo = new MainEntityRepo(this);
        mWeatherWithMainRepo = new WeatherWithMainRepo(this);
        mActivityMainBinding.searchView.setActivated(true);
        mActivityMainBinding.searchView.setQueryHint(getString(R.string.city_name));
        mActivityMainBinding.searchView.onActionViewExpanded();
        mActivityMainBinding.searchView.setIconified(false);
        mActivityMainBinding.searchView.clearFocus();
        mActivityMainBinding.searchView.setOnQueryTextListener(this);
    }

    private void apiResponse() {

        /*Initialize service call*/
        mWeatherService = ((WeatherApplication) getApplication()).getmApiConnectionModlue().getWeatherService();

        /*Weather Api Response from ViewModel*/
        mWeatherViewModel.getmWeatherModel().observe(this, new Observer<WeatherModel>() {
            @Override
            public void onChanged(WeatherModel weatherModel) {
                Log.d(TAG, "My Weather Model data is " + weatherModel.getmWeather().size());

                /*we are checking the weather id is exisiting or not in database*/
                List<WeatherRoomModel> weatherList = mWeatherEntityRepo.getWeatherById(weatherModel.getmId());
                if (weatherList.size() > 0) {
                    /*Delete the weather id in that particular table if id exists*/
                    mWeatherEntityRepo.deleteById(weatherModel.getmId());
                    mMainEntityRepo.deleteByWeatherId(weatherModel.getmId());
                }

                /*save server response into database*/
                saveWeatherEntityData(weatherModel);
                saveMainEnityData(weatherModel.getmMain(), weatherModel.getmId(), weatherModel.getmName());
            }
        });

        /*Handling error model if search city name is not available*/
        mWeatherViewModel.getmErrorModel().observe(this, new Observer<Throwable>() {
            @Override
            public void onChanged(Throwable throwable) {
                Log.d(TAG, "My Error message " + throwable.getMessage());
                showToast(getString(R.string.city_not_found));
            }
        });
    }

    private void dbOperations() {

        /* Monitoring the database events*/
        mWeatherWithMainRepo.getWeatherWithMain().observe(this, new Observer<List<WeatherWithMain>>() {
            @Override
            public void onChanged(List<WeatherWithMain> weatherWithMains) {
                Log.d(TAG, "My weatherWithMains is " + weatherWithMains.size());
                mWeatherList.clear();

                for (WeatherWithMain weatherWithMain : weatherWithMains) {
                    mWeatherList.add(weatherWithMain.mWeatherRoomModel);

                    for ( int i = 0; i < weatherWithMain.mMainRoomList.size() ; i++){
                        Log.d(TAG, "My Temp is "+weatherWithMain.mMainRoomList.get(i).mTemp);
                        mTempList.put(weatherWithMain.mWeatherRoomModel.name, weatherWithMain.mMainRoomList.get(i).mTemp);
                    }

                }



                mWeatherAdapter.notifyDataSetChanged();

                if (weatherWithMains.size() == 0) {
                    mActivityMainBinding.emptyView.setVisibility(View.VISIBLE);
                    mActivityMainBinding.recyclerView.setVisibility(View.GONE);
                } else {
                    mActivityMainBinding.emptyView.setVisibility(View.GONE);
                    mActivityMainBinding.recyclerView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void initAdapter() {
        setRecyclerView();
        mWeatherAdapter = new WeatherAdapter(this, mWeatherList, mTempList);
        mActivityMainBinding.recyclerView.setAdapter(mWeatherAdapter);
    }

    private void setRecyclerView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mActivityMainBinding.recyclerView.setLayoutManager(mLayoutManager);
        mActivityMainBinding.recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public boolean onQueryTextSubmit(String cityName) {
        Log.d(TAG, "On Query Text Submit " + cityName);

        /*API call for that particular city where user enter the city in searchView after tapping on search in keyboard*/
        if (Connectivity.isConnected(this)) {
            mWeatherViewModel.getWeatherData(mWeatherService, cityName);
        } else {
            showToast(getString(R.string.no_internet_msg));
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        Log.d(TAG, "On Query Text Change " + s);

        /*Filtering the list based on the user input text*/
        List<WeatherWithMain> mWeatherListMain = mWeatherWithMainRepo.getWeatherMainByName(s + "%");
        mWeatherList.clear();

        for (WeatherWithMain weatherWithMain : mWeatherListMain) {

            mWeatherList.add(weatherWithMain.mWeatherRoomModel);
        }
        mWeatherAdapter.getFilterByName(mWeatherList, s);

        if (mWeatherList.size() > 0)
            Log.d(TAG, "My Weather list data is " + mWeatherList.size());
        return false;
    }


    /**
     * Insert data in Weather Table
     *
     * @param model
     */
    private void saveWeatherEntityData(WeatherModel model) {
        WeatherRoomModel mWeatherEntity = new WeatherRoomModel(model.getmId(), model.getmName());
        mWeatherEntityRepo.insert(mWeatherEntity);
    }

    /**
     * Insert data in Main Table
     *
     * @param main
     * @param id
     * @param name
     */
    private void saveMainEnityData(Main main, int id, String name) {
        MainRoomModel mMainEntity = new MainRoomModel(id,
                main.getmTemp(), main.getmPressure(), main.getmHumidity(), main.getmTempMin(), main.getmTempMax(), id, name);

        mMainEntityRepo.insert(mMainEntity);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                logout();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        runOnUiThread(() -> alertDialogView = DialogUtils.showDialogWithYesOrNo(mActivity, getString(R.string.logout_title), getString(R.string.logout_message), getString(R.string.yes), getString(R.string.no), v -> {
            if (alertDialogView != null) {
                alertDialogView.dismiss();
            }
            clearDb();
        }, view -> {
            if (alertDialogView != null) {
                alertDialogView.dismiss();
            }
        }));

    }

    private void clearDb() {
        mMainEntityRepo.deleteAllMainTable();
        mWeatherEntityRepo.deleteAllWeatherTable();
        WeatherDatabase.clearDatabaseInstance();
        startActivity(new Intent(this, WeatherActivity.class));

    }

    @Override
    protected void onNetworkConnectionChanged(boolean isConnected) {
        Log.d(TAG, "NetworkConnection" + isConnected);
        if (!isConnected) {
            showToast(getString(R.string.internet_changed));
        }
    }

    public void showToast(String text) {
        Toast toast = Toast.makeText(mActivity, text, Toast.LENGTH_LONG);
        View view = toast.getView();
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            view.setBackgroundColor(getColor(R.color.cerulean));
        }*/
        TextView textView = (TextView) view.findViewById(android.R.id.message);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            textView.setShadowLayer(8, 4, 4, getColor(R.color.colorAccent));
        }
        textView.setPadding(8, 10, 8, 10);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            textView.setBackground(getDrawable(R.drawable.layout_backgroundblue));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            textView.setTextColor(getColor(R.color.editTextColor));
        }
        textView.setTextSize(12);
        toast.show();
    }
}

