package com.dell.myapplication.viewModel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.dell.myapplication.constants.AppConstants;
import com.dell.myapplication.model.WeatherModel;
import com.dell.myapplication.network.WeatherService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class WeatherViewModel extends ViewModel {
    private CompositeDisposable disposable = new CompositeDisposable();
    private static final String TAG = WeatherViewModel.class.getSimpleName();
    private MutableLiveData<WeatherModel> mWeatherModel = new MutableLiveData<>();
    private MutableLiveData<Throwable> mErrorModel = new MutableLiveData<>();

    public void setmWeatherModel(WeatherModel mWeatherModel) {
        this.mWeatherModel.postValue(mWeatherModel);
    }

    public void setmErrorModel(Throwable throwable) {
        this.mErrorModel.postValue(throwable);
    }

    public MutableLiveData<Throwable> getmErrorModel() {
        return mErrorModel;
    }

    public MutableLiveData<WeatherModel> getmWeatherModel() {
        return mWeatherModel;
    }

    /**
     * @param mService
     * @param cityName
     */
    public void getWeatherData(WeatherService mService, String cityName) {

        DisposableObserver<WeatherModel> observer = getObeserver();

        disposable.add(mService.getWeatherDetails(cityName, AppConstants.API_KEY)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(observer));

    }

    /**
     * @return
     */
    public DisposableObserver<WeatherModel> getObeserver() {
        return new DisposableObserver<WeatherModel>() {
            @Override
            public void onNext(WeatherModel weatherModel) {
                Log.d(TAG, "On Success WeatherModel model is " + weatherModel.toString());
                setmWeatherModel(weatherModel);
            }

            @Override
            public void onError(Throwable e) {
                Log.e(">>>>>", e.getLocalizedMessage());
                setmErrorModel(e);
            }

            @Override
            public void onComplete() {
                Log.e(">>>>>", "oncomplated...");
            }
        };
    }
}
