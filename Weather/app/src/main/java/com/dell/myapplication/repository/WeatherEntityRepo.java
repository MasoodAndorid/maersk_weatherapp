package com.dell.myapplication.repository;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.dell.myapplication.dao.WeatherDao;
import com.dell.myapplication.database.WeatherDatabase;
import com.dell.myapplication.roomModel.WeatherRoomModel;

import java.util.List;

public class WeatherEntityRepo {

    private WeatherDao mWeatherDao;
    private WeatherDatabase mWeatherDatabase;

    public WeatherEntityRepo(Context mContext) {
        mWeatherDatabase = WeatherDatabase.getAppDatabase(mContext);
        mWeatherDao = mWeatherDatabase.getWeatherDao();
    }

    /**
     * @param model
     */

    public void insert(final WeatherRoomModel model) {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                mWeatherDao.insert(model);
                return true;
            }
        }.execute();
    }

    /**
     * @param model
     */
    public void delete(final WeatherRoomModel model) {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                mWeatherDao.delete(model);
                return true;
            }
        }.execute();
    }

    /**
     *
     */
    public void deleteAllWeatherTable() {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                mWeatherDao.deleteAll();
                return true;
            }
        }.execute();
    }

    /**
     * @param model
     */
    public void update(final WeatherRoomModel model) {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                mWeatherDao.update(model);
                return true;
            }
        }.execute();
    }

    /**
     * @param id
     * @return
     */
    public List<WeatherRoomModel> getWeatherById(int id) {
        return mWeatherDao.getWeatherById(id);
    }

    /**
     * @return
     */
    public LiveData<List<WeatherRoomModel>> getWeather() {
        return mWeatherDao.getAllWeather();
    }

    /**
     * @param id
     */
    public void deleteById(final int id) {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                mWeatherDao.deleteWeatherById(id);
                return true;
            }
        }.execute();
    }

    /**
     * @param name
     * @return
     */
    public LiveData<List<WeatherRoomModel>> getWeatherByName(String name) {
        return mWeatherDao.getWeatherByName(name);
    }

}
