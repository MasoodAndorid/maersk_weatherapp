package com.dell.myapplication.roomModel;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "main"/*, foreignKeys = @ForeignKey(entity = WeatherRoomModel.class,
        parentColumns = "id", childColumns = "weatherId", onDelete = CASCADE)*/)
public class MainRoomModel {

    @PrimaryKey
    public int id;

    @ColumnInfo(name="temp")
    public double mTemp;

    @ColumnInfo(name="pressure")
    public double mPressure;

    @ColumnInfo(name = "humidity")
    public int mHumidity;

    @ColumnInfo(name = "temp_min")
    public double mTempMin;

    @ColumnInfo(name = "temp_max")
    public double mTempMax;

    @ColumnInfo(name="weatherId")
    public int mWeatherId;

    @ColumnInfo(name="weatherName")
    public String mWeatherName;

    public MainRoomModel(int id, double mTemp, double mPressure, int mHumidity,
                         double mTempMin, double mTempMax, int mWeatherId, String mWeatherName){
        this.id = id;
        this.mTemp = mTemp;
        this.mPressure = mPressure;
        this.mHumidity = mHumidity;
        this.mTempMin = mTempMin;
        this.mTempMax = mTempMax;
        this.mWeatherId = mWeatherId;
        this.mWeatherName = mWeatherName;
    }

}
