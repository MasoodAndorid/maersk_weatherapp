package com.dell.myapplication.repository;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.dell.myapplication.database.WeatherDatabase;
import com.dell.myapplication.dao.WeatherWithMain;
import com.dell.myapplication.dao.WeatherWithMainDao;

import java.util.List;

public class WeatherWithMainRepo {

    private WeatherWithMainDao mWeatherWithMainDao;
    private WeatherDatabase mWeatherDatabase;


    public WeatherWithMainRepo(Context mContext) {
        mWeatherDatabase = WeatherDatabase.getAppDatabase(mContext);
        mWeatherWithMainDao = mWeatherDatabase.getWeatherWithMainDao();
    }

    /**
     * @return
     */
    public LiveData<List<WeatherWithMain>> getWeatherWithMain() {
        return mWeatherWithMainDao.getWeatherWithMain();
    }

    /**
     * @param id
     * @return
     */
    public List<WeatherWithMain> getWeatherMainById(int id) {
        return mWeatherWithMainDao.getWeatherById(id);
    }

    /**
     * @param name
     * @return
     */
    public List<WeatherWithMain> getWeatherMainByName(String name) {
        return mWeatherWithMainDao.getWeatherByName(name);
    }


}
