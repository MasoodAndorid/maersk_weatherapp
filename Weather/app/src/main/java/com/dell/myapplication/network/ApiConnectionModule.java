package com.dell.myapplication.network;

import com.dell.myapplication.BuildConfig;
import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.dell.myapplication.constants.AppConstants.BASE_URL;
import static com.dell.myapplication.constants.AppConstants.CONNECTION_TIME_OUT;
import static com.dell.myapplication.constants.AppConstants.READ_TIME_OUT;
import static com.dell.myapplication.constants.AppConstants.WRITE_TIME_OUT;

public class ApiConnectionModule {
    private static volatile ApiConnectionModule retrofitManager;
    private static Retrofit retrofit;
    private static String mBaseUrl = "";
    private static WeatherService mWeatherService;
    private static OkHttpClient okHttpClient;

    public static ApiConnectionModule getInstance() {
        if (null == retrofitManager) {
            synchronized (ApiConnectionModule.class) {
                if (retrofitManager == null) {
                    retrofitManager = new ApiConnectionModule();
                    mWeatherService = retrofit.create(WeatherService.class);
                }
            }
        }
        return retrofitManager;
    }

    public ApiConnectionModule() {
        if (okHttpClient == null) {
            initOkHttp();
        }
        retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private static void initOkHttp() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final javax.net.ssl.SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            httpClient.sslSocketFactory(sslSocketFactory);
            httpClient.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(httpLoggingInterceptor);
        }
        httpClient.addNetworkInterceptor(new StethoInterceptor());
        httpClient.connectTimeout(CONNECTION_TIME_OUT, TimeUnit.SECONDS);
        httpClient.readTimeout(READ_TIME_OUT, TimeUnit.SECONDS);
        httpClient.writeTimeout(WRITE_TIME_OUT, TimeUnit.SECONDS);
        okHttpClient = httpClient.build();
    }

    public WeatherService getWeatherService() {
        return mWeatherService;
    }
}
