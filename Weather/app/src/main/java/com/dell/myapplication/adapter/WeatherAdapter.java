package com.dell.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filterable;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.dell.myapplication.R;
import com.dell.myapplication.databinding.SingleNameTypeBinding;
import com.dell.myapplication.roomModel.WeatherRoomModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.ViewHolder> implements Filterable {

    private static final String TAG = WeatherAdapter.class.getSimpleName();
    private Context mContext;
    private List<WeatherRoomModel> mTypesOfNames;
    private List<WeatherRoomModel> original;
    HashMap<String, Double> mTempList;

    /**
     * @param context
     * @param typesOfNames
     */
    public WeatherAdapter(Context context, List<WeatherRoomModel> typesOfNames, HashMap<String, Double> mTempList) {
        this.mContext = context;
        this.mTypesOfNames = typesOfNames;
        this.original = new ArrayList<>();
        this.original.addAll(typesOfNames);
        this.mTempList = mTempList;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder((SingleNameTypeBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.single_name_type, parent, false));
    }

    /**
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        SingleNameTypeBinding nameTypeBinding = holder.getSingleNameTypeBinding();
        nameTypeBinding.cityName.setText(mTypesOfNames.get(holder.getAdapterPosition()).name != null ? mTypesOfNames.get(holder.getAdapterPosition()).name : " ");

        nameTypeBinding.cityTemparature.setText(mTempList.get(mTypesOfNames.get(holder.getAdapterPosition()).name)!=null ? ": " + mTempList.get(mTypesOfNames.get(holder.getAdapterPosition()).name) +" C": "");
    }

    @Override
    public int getItemCount() {
        if (mTypesOfNames.size() > 0) {
            return mTypesOfNames.size();
        } else return 0;
    }

    /**
     * @param mWeatherList
     * @param s
     */
    public void getFilterByName(List<WeatherRoomModel> mWeatherList, CharSequence s) {

        this.mTypesOfNames = mWeatherList;
        getFilter().filter(s);
    }

    @Override
    public Filter getFilter() {
        return new Filter();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private SingleNameTypeBinding singleNameTypeBinding;

        /**
         * @param singleNameTypeBinding
         */
        public ViewHolder(SingleNameTypeBinding singleNameTypeBinding) {
            super(singleNameTypeBinding.getRoot());
            this.singleNameTypeBinding = singleNameTypeBinding;
            singleNameTypeBinding.executePendingBindings();
        }

        public SingleNameTypeBinding getSingleNameTypeBinding() {
            return singleNameTypeBinding;
        }
    }

    public class Filter extends android.widget.Filter {

        /**
         * @param charSequence
         * @return
         */
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {

            if (charSequence.length() == 0) {
                mTypesOfNames.addAll(original);
            } else {
                for (WeatherRoomModel nameList : original) {
                    if (Pattern.compile(Pattern.quote(charSequence.toString().trim()),
                            Pattern.CASE_INSENSITIVE).matcher(nameList.name).find()) {
                        if (!mTypesOfNames.contains(nameList))
                            mTypesOfNames.add(nameList);
                    }

                }
            }
            return null;
        }

        /**
         * @param charSequence
         * @param filterResults
         */
        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            notifyDataSetChanged();
        }
    }
}
