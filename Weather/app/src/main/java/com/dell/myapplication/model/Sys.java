package com.dell.myapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sys {
    @SerializedName("country")
    @Expose
    private String mCountry;
    @SerializedName("id")
    @Expose
    private Integer mId;
    @SerializedName("message")
    @Expose
    private Double mMessage;
    @SerializedName("sunrise")
    @Expose
    private Integer mSunrise;
    @SerializedName("sunset")
    @Expose
    private Integer mSunset;
    @SerializedName("type")
    @Expose
    private Integer mType;

    public String getmCountry() {
        return mCountry;
    }

    public void setmCountry(String mCountry) {
        this.mCountry = mCountry;
    }

    public Integer getmId() {
        return mId;
    }

    public void setmId(Integer mId) {
        this.mId = mId;
    }

    public Double getmMessage() {
        return mMessage;
    }

    public void setmMessage(Double mMessage) {
        this.mMessage = mMessage;
    }

    public Integer getmSunrise() {
        return mSunrise;
    }

    public void setmSunrise(Integer mSunrise) {
        this.mSunrise = mSunrise;
    }

    public Integer getmSunset() {
        return mSunset;
    }

    public void setmSunset(Integer mSunset) {
        this.mSunset = mSunset;
    }

    public Integer getmType() {
        return mType;
    }

    public void setmType(Integer mType) {
        this.mType = mType;
    }
}
