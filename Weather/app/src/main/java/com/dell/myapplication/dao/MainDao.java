package com.dell.myapplication.dao;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.dell.myapplication.roomModel.MainRoomModel;

import java.util.List;

import retrofit2.http.DELETE;

@Dao
public interface MainDao {

    @Insert
    void insert(MainRoomModel... mMainRoomModel);

    @Delete
    void delete(MainRoomModel... mMainRoomModel);

    @Update
    void update(MainRoomModel... mMainRoomModel);

    @Query("SELECT * FROM main")
    List<MainRoomModel> getAllMain();

    @Query("SELECT * FROM main WHERE weatherId=:mWeatherId")
    List<MainRoomModel> findMainForWeather(int mWeatherId);

    @Query("DELETE FROM main WHERE weatherId= :mWeatherId")
    void deleteWeatherById(int mWeatherId);

    @Query("DELETE FROM main")
    void deleteAll();
}
