package com.dell.myapplication.model;

import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class WeatherModel {
    @SerializedName("base")
    @Expose
    private String mBase;
    @SerializedName("clouds")
    @Expose
    private Clouds mClouds;
    @SerializedName("cod")
    @Expose
    private Integer mCod;
    @SerializedName("coord")
    @Expose
    private Coord mCoord;

    @SerializedName("dt")
    @Expose
    private Integer mDt;

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private Integer mId;

    @SerializedName("main")
    @Expose
    private Main mMain;
    @SerializedName("name")
    @Expose
    private String mName;

    @SerializedName("sys")
    @Expose
    private Sys mSys;

    @SerializedName("timezone")
    @Expose
    private Integer mTimezone;

    @SerializedName("weather")
    @Expose
    private List<Weather> mWeather = null;

    @SerializedName("wind")
    @Expose
    private Wind mWind;

    public String getmBase() {
        return mBase;
    }

    public void setmBase(String mBase) {
        this.mBase = mBase;
    }

    public Clouds getmClouds() {
        return mClouds;
    }

    public void setmClouds(Clouds mClouds) {
        this.mClouds = mClouds;
    }

    public Integer getmCod() {
        return mCod;
    }

    public void setmCod(Integer mCod) {
        this.mCod = mCod;
    }

    public Coord getmCoord() {
        return mCoord;
    }

    public void setmCoord(Coord mCoord) {
        this.mCoord = mCoord;
    }

    public Integer getmDt() {
        return mDt;
    }

    public void setmDt(Integer mDt) {
        this.mDt = mDt;
    }

    public Integer getmId() {
        return mId;
    }

    public void setmId(Integer mId) {
        this.mId = mId;
    }

    public Main getmMain() {
        return mMain;
    }

    public void setmMain(Main mMain) {
        this.mMain = mMain;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public Sys getmSys() {
        return mSys;
    }

    public void setmSys(Sys mSys) {
        this.mSys = mSys;
    }

    public Integer getmTimezone() {
        return mTimezone;
    }

    public void setmTimezone(Integer mTimezone) {
        this.mTimezone = mTimezone;
    }

    public List<Weather> getmWeather() {
        return mWeather;
    }

    public void setmWeather(List<Weather> mWeather) {
        this.mWeather = mWeather;
    }

    public Wind getmWind() {
        return mWind;
    }

    public void setmWind(Wind mWind) {
        this.mWind = mWind;
    }
}
