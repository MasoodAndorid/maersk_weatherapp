package com.dell.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Embedded;
import androidx.room.Relation;

import com.dell.myapplication.roomModel.MainRoomModel;
import com.dell.myapplication.roomModel.WeatherRoomModel;

import java.util.List;

public class WeatherWithMain {

    @Embedded
    public WeatherRoomModel mWeatherRoomModel;

    @Relation(parentColumn = "id", entityColumn = "weatherId")
    public List<MainRoomModel> mMainRoomList;

    @Relation(parentColumn = "name", entityColumn = "weatherName")
    List<MainRoomModel> mMainWNameList;
}
