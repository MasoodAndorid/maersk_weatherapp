package com.dell.myapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Coord {
    @SerializedName("lat")
    @Expose
    private Double mLat;
    @SerializedName("lon")
    @Expose
    private Double mLon;

    public Double getmLat() {
        return mLat;
    }

    public void setmLat(Double mLat) {
        this.mLat = mLat;
    }

    public Double getmLon() {
        return mLon;
    }

    public void setmLon(Double mLon) {
        this.mLon = mLon;
    }
}
