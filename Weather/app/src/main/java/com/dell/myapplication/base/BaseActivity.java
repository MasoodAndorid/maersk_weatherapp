package com.dell.myapplication.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import java.io.File;


public abstract class BaseActivity extends AppCompatActivity {

    private static final String TAG = BaseActivity.class.getCanonicalName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

    }
    private boolean isReceiverRegistered;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = null;
            if (cm != null) {
                activeNetwork = cm.getActiveNetworkInfo();
            }
            boolean isConnected = false;
            if (activeNetwork != null) {
                isConnected = activeNetwork.isConnected();
                Log.d(TAG, "Mobile Internet2 connected: " + activeNetwork.isConnected());
            }

            onNetworkConnectionChanged(isConnected);
            Log.d(TAG, "isconnected: " + isConnected);
        }
    };

    protected abstract void onNetworkConnectionChanged(boolean isConnected);
    public void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
           Log.e(TAG, e.getMessage());
        }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (String aChildren : children) {
                boolean success = deleteDir(new File(dir, aChildren));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

   /* public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        isReceiverRegistered = true;
    }

    @Override
    protected void onPause() {
        if (isReceiverRegistered) {
            unregisterReceiver(broadcastReceiver);
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        try {
            trimCache(this);
        } catch (Exception e) {
           Log.e(TAG, e.getMessage());
        }
        super.onDestroy();
    }

}
