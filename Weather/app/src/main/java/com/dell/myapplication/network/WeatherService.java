package com.dell.myapplication.network;

import com.dell.myapplication.model.WeatherModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherService {

    /*Weather api based on city name*/
    @GET("data/2.5/weather")
    Observable<WeatherModel> getWeatherDetails(@Query("q") String cityname, @Query("appid") String apiKey);

}
