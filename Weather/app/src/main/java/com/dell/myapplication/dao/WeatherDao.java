package com.dell.myapplication.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.dell.myapplication.roomModel.WeatherRoomModel;

import java.util.List;

@Dao
public interface WeatherDao {

    @Insert
    void insert(WeatherRoomModel... mWeatherRoomModel);

    @Update
    void update(WeatherRoomModel... mWeatherRoomModel);

    @Delete
    void delete(WeatherRoomModel... mWeatherRoomModel);

    @Query("SELECT * FROM weather")
    LiveData<List<WeatherRoomModel>> getAllWeather();

    @Query("SELECT * FROM weather WHERE id = :id")
    List<WeatherRoomModel> getWeatherById(int id);

    @Query("SELECT * FROM weather WHERE name = :name")
    LiveData<List<WeatherRoomModel>> getWeatherByName(String name);

    @Query("DELETE FROM weather WHERE id= :id")
    void deleteWeatherById(int id);

    @Query("DELETE FROM weather")
    void deleteAll();
}
