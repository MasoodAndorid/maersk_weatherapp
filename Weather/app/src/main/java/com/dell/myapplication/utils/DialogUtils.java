package com.dell.myapplication.utils;

import android.app.Activity;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.dell.myapplication.R;
import com.dell.myapplication.databinding.DialogYesNoBinding;

public class DialogUtils {

    public static AlertDialog showDialogWithYesOrNo(final Activity activity, final String title, final String message, String positiveMsg, String negativeMsg, View.OnClickListener PosOnClickListener, View.OnClickListener NegOnClickListener) {
        final DialogYesNoBinding binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.dialog_yes_no, null, false);
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(binding.getRoot());
        final AlertDialog alertDialog = builder.show();
        binding.title.setText(title);
        binding.message.setText(message);
        binding.positiveText.setText(positiveMsg);
        binding.negativeText.setText(negativeMsg);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        binding.positiveText.setOnClickListener(PosOnClickListener);
        binding.negativeText.setOnClickListener(NegOnClickListener);
        return alertDialog;
    }
}
