package com.dell.myapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Wind {
    @SerializedName("deg")
    @Expose
    private Double mDeg;
    @SerializedName("speed")
    @Expose
    private Double mSpeed;

    public Double getmDeg() {
        return mDeg;
    }

    public void setmDeg(Double mDeg) {
        this.mDeg = mDeg;
    }

    public Double getmSpeed() {
        return mSpeed;
    }

    public void setmSpeed(Double mSpeed) {
        this.mSpeed = mSpeed;
    }
}
