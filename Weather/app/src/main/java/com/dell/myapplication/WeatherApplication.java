package com.dell.myapplication;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.dell.myapplication.network.ApiConnectionModule;
import com.facebook.stetho.Stetho;


public class WeatherApplication extends MultiDexApplication {

    private ApiConnectionModule mApiConnectionModlue;

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        Stetho.initializeWithDefaults(this);
        mApiConnectionModlue = ApiConnectionModule.getInstance();
    }

    public ApiConnectionModule getmApiConnectionModlue() {
        return mApiConnectionModlue;
    }


}
